﻿namespace GROUP4.ApartmentType.Test.Helper.DTO
{
    public class ProcedureParamInfo
    {
        public string PARAMETER_NAME { get; set; }
        public string PARAMETER_MODE { get; set; }
        public string DATA_TYPE { get; set; }
        public int CHARACTER_MAXIMUM_LENGTH { get; set; }
    }
}
