﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using GROUP4.Apartment.Test.Helper;
using System.Linq;
using GROUP4.Apartment.Test.DTO;

namespace GROUP4.Apartment.Test
{
    [TestClass]
    public class UpdateApartment : Load
    {
        public UpdateApartment()
        {
            driver = new ChromeDriver();
            homeURL = "http://localhost:4200";
        }

        [TestInitialize]
        public void InitializeTest()
        {
            Login();
        }

        [TestCleanup]
        public void CleanTest()
        {
            driver.Close();
        }
    }
}
